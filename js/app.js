require.config({
	catchError: {
		define: true
	}
});

require(['lib/renderer', 'lib/cytoscape.min'],
	function(renderer, cytoscape) {
		
		renderer(cytoscape);
		
		var cy = cytoscape({
			container: document.getElementById('cy'),
			renderer: {
				name: "css"
			},
			style: [{
				selector: 'node',
				style: {
					'shape': 'roundrectangle',
					'background-color': '#666',
					'label': 'data(name)',
					'text-valign': 'center',
					'text-halign': 'center',
					'width': 'label',
					'height': 'label',
					'padding': '8px'
				}
			}, {
				selector: 'edge',
				style: {
					'width': 3,
					'line-color': '#ccc',
					'target-arrow-color': '#ccc',
					'target-arrow-shape': 'triangle'
				}
			}]
		});
		
		cy.add([
			{ group: "nodes", data: { id: "n0", name: "H<sub>2</sub>O" }, position: { x: 100, y: 100 } },
			{ group: "nodes", data: { id: "n1", name: "NaCl" }, position: { x: 200, y: 200 } },
			{ group: "edges", data: { id: "e0", source: "n0", target: "n1" } }
		]);
	}
);